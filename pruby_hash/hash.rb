module PRuby

  ############################################################
  #
  # Definition des methodes paralleles qui s'appliquent
  # directement (et uniquement) a des Hash. 
  #
  # Les methodes publiques sont les suivantes:
  # - peach_key
  # - peach_value
  # - peach_pair
  #
  # Ces methodes sont definies dans le present module, mais ensuite
  # la classe Hash est etendue avec ces methodes (vie
  # des includes).
  #
  ############################################################

  module PHash

    # Execute un bloc de code sur les valeurs de chaque clé d'un Hash
    def peach_value( opts = {}, &b )
        DBC.require( self.class == Hash, "Hash uniquement." ) 
        self.class.module_eval { attr_accessor :_range}
        self._range = self.values()

        __pforall__( self._range, false, opts, &b )
    end

    # Execute un bloc de code sur chaque clé d'un Hash
    def peach_key( opts = {}, &b )
        DBC.require( self.class == Hash, "Hash uniquement." ) 
        self.class.module_eval { attr_accessor :_range}
        self._range = self.keys()

        __pforall__( self._range, false, opts, &b )
    end

    # Execute un bloc de code sur chaque clé d'un Hash
    def peach_pair( opts = {}, &b )
        DBC.require( self.class == Hash, "Hash uniquement." ) 
        self.class.module_eval { attr_accessor :_range}
        self._range = self.keys()

        __pforall__( self._range, true, opts, &b )
    end

    private

    # @!visibility private
    # Determine les bornes (inferieure et superieure) d'une tranche d'elements
    # adjacents,
    #
    # @param [Fixnum] k Indice de la tranche
    # @param [Fixnum] n Nombre d'elements a repartir
    # @param [Fixnum] nb_threads Nombre de threads entre lesquels repartir les elements
    # @return [Array<Fixnum, Fixnum>] Bornes inferieure et superieure (inclusives) de la tranche
    #
    def __bornes_de_tranche__( k, n, nb_threads )
      nb_min_par_thread = n / nb_threads
      nb_a_distribuer = n % nb_threads

      b_inf = k * nb_min_par_thread + [k, nb_a_distribuer].min
      b_sup = (k+1) * nb_min_par_thread + [k+1, nb_a_distribuer].min - 1
      [b_inf, b_sup]
    end

    def __analyser_arguments__( resultat, opts )

      nb_threads = __nombre_de_threads__( opts[:nb_threads], size )
      tt_statique, tt_dynamique = opts[:static], opts[:dynamic]
      resultat = (resultat.eql? self) ? nil : resultat
      DBC.require( tt_statique.nil? || tt_dynamique.nil?,
                   "*** On ne peut pas specifier a la fois statique et dynamique" )

      method = nil
      if tt_dynamique
        tt_dynamique = 1 if tt_dynamique == true
        DBC.require( tt_dynamique > 0,
                     "*** La taille des taches doit etre un entier superieure a 0" )
        method = :fork_and_wait_threads_dynamique
        opts = [tt_dynamique, nb_threads, resultat]
      else
        adjacent = true # Jusqu'a preuve du contraire, donc y compris si "static: true" ou pas defini
        if tt_statique && tt_statique != true
          DBC.require( tt_statique.class == Fixnum && tt_statique > 0,
                       "*** La taille des taches doit etre un entier superieur a 0" )
          adjacent = nb_threads * tt_statique > size
        end
        if adjacent
          method = :fork_and_wait_threads_adj
          opts = [nb_threads, resultat]
        else
          method = :fork_and_wait_threads_cyclique
          opts = [tt_statique, nb_threads, resultat]
        end
      end
      [method, opts]
    end

    # Itere un bloc, en parallele, sur les differents elements d'un tableau.
    #
    # @option (see #peach)
    # @param [Array] resultat Dans quel tableau doivent etre conserves les elements du resultat
    # @param [Bool] sur_pair true si on applique le bloc sur les index, false si on applique sur les elements
    # @param b Le bloc a executer
    #
    # @return Le tableau initial si resultat.eql? self, sinon un autre tableau: voir les methodes publiques
    #
    # @raise [DBC::Failure] Divers conditions selon les options
    #
    def __pforall__( resultat, sur_pair, opts = {}, &b )
      return resultat if size == 0

      method, opts = __analyser_arguments__( resultat, opts )

      # On applique maintenant la methode d'iteration approprie a
      # chacun des elements de l'Array, en yieldant au bloc recu en
      # argument.
      send method, *opts, sur_pair, &b

      resultat
    end

    def fork_and_wait_threads_adj( nb_threads, resultat = nil, sur_pair = nil )
      PRuby.nb_threads_used = nb_threads

      threads = (0...nb_threads).map do |k|
        b_inf, b_sup = __bornes_de_tranche__( k, size, nb_threads )
        Thread.new(b_inf, b_sup) do |inf, sup|
          Thread.current[:thread_index] = k
          (inf..sup).each do |i|
            r = yield( sur_pair ? [self._range[i], self[self._range[i]]] : self._range[i] )
            resultat[i] = r if resultat
            r
          end
        end
      end
      threads.map(&:join)
    end

    def fork_and_wait_threads_dynamique( taille_tache, nb_threads, resultat = nil, sur_pair = nil )
      pool = ForkJoin::Pool.new nb_threads
      PRuby.nb_threads_used = pool.parallelism

      tasks = (0...size).step(taille_tache).map do |k|
        PRubyArraySliceTask.new( self, k, [size, k+taille_tache].min-1 ) do |i|
          r = yield( sur_pair ? [self._range[i], self[self._range[i]]] : self._range[i] )
          resultat[i] = r if resultat
          r
        end
      end

      PRuby.incr_nb_tasks_created size
      pool.invoke_all( tasks )
    end

    def fork_and_wait_threads_cyclique( taille_tache, nb_threads, resultat = nil, sur_pair = nil )
      PRuby.nb_threads_used = nb_threads

      threads = (0...nb_threads).map do |num_t|
        Thread.new(num_t, taille_tache, nb_threads, size) do |k, tt, nb_thrs, n|
          step = nb_thrs * tt

          (k*tt..n).step(step).each do |bloc|
            # On traite un bloc (tache) d'au plus tt (taille_tache)
            (bloc...[bloc+tt,n].min).each do |j|
              r = yield( sur_pair ? [self._range[j], self[self._range[j]]] : self._range[j] )
              resultat[j] = r if resultat
              r
            end
          end
        end
      end
      threads.map(&:join)
    end

    def __nombre_de_threads__( nb_threads, n )
      DBC.require( nb_threads.nil? || nb_threads > 0,
                   "*** Le nombre de threads specifie doit etre superieur a 0" )

      nb_threads ||= PRuby.nb_threads

      [nb_threads, n].min
    end
  end

  # On etend le module Hash avec les methodes ainsi definies.
  Hash.send(:include, PRuby::PHash)

  # @!visibility private
  # Class auxiliaire, privee, utilisee pour les ForkJoin::Task de la
  # bibliotheque jruby/Java.
  class PRubyArraySliceTask < ForkJoin::Task
    def initialize( a, i, j, &block )
      @a, @i, @j, @block = a, i, j, block
    end

    def call
      (@i..@j).each do |k|
        @a.instance_exec(k, &@block)
      end
    end
  end
end
