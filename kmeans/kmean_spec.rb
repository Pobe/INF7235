#require_relative "kmean"
#$kclass = Kmeans
#require_relative "kmean_static"
#$kclass = KmeansStatic
require_relative "kmean_static_fj"
$kclass = KmeansStaticFJ
#require_relative "kmean_dynamic"
#$kclass = KmeansDynamic
require "test/unit"

# (GT) Dans assert_equal, le premier argument doit etre la valeur *attendue*!

class TestPoint < Test::Unit::TestCase
  def test_EuclideanDistance_Valid
    pointA = Point.new(2, 2)
    pointB = Point.new(5, 5)

    assert_in_delta 4.2426, pointA.euclidean_distance(pointB)
  end

  def test_EuclideanDistance_ZeroValid
    pointA = Point.new(0,0)
    pointB = Point.new(0,0)

    assert_in_delta 0, pointA.euclidean_distance(pointB)
  end
end

class TestKmean < Test::Unit::TestCase
  def test_AveragePoint_Valid
    pointA = Point.new(2, 2)
    pointB = Point.new(6, 6)
    pointC = Point.new(4, 1)
    k = $kclass.new()
    averagePoint = k.average_point([pointA, pointB, pointC])

    assert_equal Point.new(4, 3), averagePoint
  end

  def test_AveragePoint_FloatValid
    pointA = Point.new(0, 0)
    pointB = Point.new(0, 0)
    pointC = Point.new(1, 1)
    k = $kclass.new()
    averagePoint = k.average_point([pointA, pointB, pointC])

    assert_in_delta 0.333, averagePoint.x, 0.001
    assert_in_delta 0.333, averagePoint.y, 0.001
  end

  def test_ChangeRatioPlain_XValid
    newPoint = Point.new(2,0)
    oldPoint = Point.new(1,0)
    k = $kclass.new()
    changeRatio = k.change_ratio(oldPoint, newPoint)

    assert_equal 100, changeRatio
  end

  def test_ChangeRatioPlain_YValid
    newPoint = Point.new(0,2)
    oldPoint = Point.new(0,1)
    k = $kclass.new()
    changeRatio = k.change_ratio(oldPoint, newPoint)

    assert_equal 100, changeRatio
  end

  def test_ChangeRatioFloat_XValid
    newPoint = Point.new(2.5,1.1)
    oldPoint = Point.new(2,1)
    k = $kclass.new()
    changeRatio = k.change_ratio(oldPoint, newPoint)

    assert_equal 25, changeRatio
  end

  def test_ChangeRatioFloatReverse_XValid
    #base on oldpoint position
    #which gives a different result from the one precedent
    oldPoint = Point.new(2.5,1.1)
    newPoint = Point.new(2,1)
    k = $kclass.new()
    changeRatio = k.change_ratio(oldPoint, newPoint)

    assert_equal 20, changeRatio
  end

  def test_kMeanBaseCentroid_MatchCentroid
    data = [
            Point.new(1,1),
            Point.new(2,2),
            Point.new(3,3),
           ]

    k = $kclass.new(kCount=3)
    k.work(data)

    # Ensures proper number of centroids
    assert_equal 3, k.centroids.size

    # Centroids shouldn't move each point matches what they are
    assert_equal Point.new(1, 1), k.centroids[0]
    assert_equal Point.new(2, 2), k.centroids[1]
    assert_equal Point.new(3, 3), k.centroids[2]
  end

  def test_KmeanAproxCentroid_MatchCentroidAndClusters
    data = [
            Point.new(1,1),
            Point.new(2,2),
            Point.new(10,10),
            Point.new(11,11),
            Point.new(30,30),
            Point.new(33,33),
           ]

    k = $kclass.new(kCount=3)
    k.work(data)

    # Ensure proper number of centroids
    assert_equal 3, k.centroids.size
    assert_equal 2, k.clusters[0].size
    assert_equal 2, k.clusters[1].size
    assert_equal 2, k.clusters[2].size

    # Check one cluster to make sure
    assert_equal Point.new(1.5, 1.5), k.centroids[0]

    assert( k.clusters[0].any? { |p| p == Point.new(1, 1) } )
    assert( k.clusters[0].any? { |p| p == Point.new(2, 2) } )
  end

  def test_KmeanLargeNumberOfDataAndClusters
    nb_points = 200
    data = (0...nb_points).map { |i| Point.new(i, i) }

    k = $kclass.new(kCount = nb_points)
    k.work(data)

    # Ensure proper number of centroids
    assert_equal nb_points, k.centroids.size
    assert_equal nb_points, k.clusters.size

    assert k.clusters.all? { |cluster| cluster.size == 1 }

    assert k.centroids.all? { |centroid| data.include?(centroid) }
    assert k.clusters.all? { |cluster| data.include?(cluster.first) }
  end

  def test_KmeanLargeNumberOfDataButOneCluster
    nb_points = 200
    data = (0...nb_points).map { |i| Point.new(i, i) }
    expected = data.reduce(Point.new(0, 0), &:+) * (1.0 / nb_points)

    k = $kclass.new(kCount = 1)
    k.work(data)

    # Ensure proper number of centroids
    assert_equal 1, k.centroids.size
    assert_equal 1, k.clusters.size

    assert k.clusters.all? { |cluster| cluster.size == nb_points }
    assert_equal expected, k.centroids.first
  end
end
