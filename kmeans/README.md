K-Means
=======
L'algorithme K-Means permet de créer des grappes d'individus qui forment une population de manière non supervisée. La non supervision signifie que l'algorithme crée des grappes sans connaître les individus qui formeront celles-ci.

L'algorithme évalue donc à l'aide d'une moyenne les points qui devraient ou non appartenir à un ou l'autre des clusters par la similarité relative.

Techniquement
=============
Il faut créer de manière arbitraire un nombre de centroïdes. Ils représentent le centre des différentes grappes.

Chaque centroïde passe à travers l'ensemble des individus et s'assigne un nombre d'individus en déplaçant sa position (ses valeurs) selon cette assignement. Ensuite il calcule la moyenne des distances des noeuds assignés et se dirige vers ce nombre.

Le processus est répété jusqu'à temps qu'il n'est plus de changement dans la grappe.

Processus
=========
1. Choisir la valeur de K
2. Définir les valeurs aléatoirement de chaque K
3. Calculer les distances de chaque centroïde pour chaque individu
4. Classifier en fonction de la distance l'ensemble de chaque centroïde
5. Faire la moyenne des distances par rapport à chaque noeuds et son ensemble
6. Répéter

## png centroids
- [107.05710659898477, 386.96192893401013]
- [422.47314049586777, 259.82851239669424]
- [105.20372670807454, 110.92422360248447]
- [372.57744360902257, 77.21353383458647]
- [356.94899536321486, 426.0077279752705]
- [237.9672667757774, 242.99345335515548]

