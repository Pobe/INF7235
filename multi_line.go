package main

import (
	"os"
	"bufio"
	"fmt"
	"strings"
	"math/rand"
	"time"
	"io"
	"bytes"
)

func main() {
	// simple()
	//lineWork() //Perte de l'ordre
	chopArray()
}

func chopArray() {
	file, _ := os.Open("small_dict")
	defer file.Close()
	i := 0
	var lines []string
	
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
		i++
	} 

	seuil := 2
	chop := i / seuil
	var matchLines [i]string 
	fmt.Println(chop)
}

func do(lineList []string) {

}

func lineCounter(r io.Reader) (int, error) {
    buf := make([]byte, 32*1024)
	count := 0
	lineSep := []byte{'\n'}
	
	for {
		c, err := r.Read(buf)
		count += bytes.Count(buf[:c], lineSep)
			
		switch {
		case err == io.EOF:
			return count, nil
		
		case err != nil:
			return count, err
		}
	}
}

func simple() {
	file, _ := os.Open("small_dict")
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		str := scanner.Text()
		if strings.Contains(str, "a") {
			fmt.Println(str)
		}
	}
}

func lineWork() {
	file, _ := os.Open("small_dict")
	defer file.Close()

	results := make(chan string)
	readCount := 0

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		str := scanner.Text()
		readCount += 1
		go con(str, results)
	}

	for i := 0; i < readCount; i++ {
		fmt.Println(<-results)
	}

}

func con(str string, results chan string) {
	jiggle()
	if strings.Contains(str, "a") {
		results <- str
	}
}

func jiggle() {
	//jiggle between 0 to 10 microsecond
	r := rand.Intn(10)
	time.Sleep(time.Duration(r) * time.Microsecond)
}
